"""
Namefile.py

Pie chart maker.
Receives a dataset,a column key of it, and the filename for the output


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import os
import source.utils.helper as ut
import shutil
from source.cleaning.extract import extract
from source.cleaning.preproc import prepoc
from source.cleaning.filter_1 import filter1
from source.cleaning.filter_2 import filter2
from source.cleaning.filter_3 import filter3
from source.cleaning.data_organization import organize
from optparse import OptionParser


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def main():

    parser = OptionParser()
    parser.add_option("-a", "--antibodies", dest="antibodies_path",
                      help="Antibodies sequences in Fasta format, the ID of the antibody should be the same as the filename of the ProtoArray data")
    parser.add_option("-l", "--lots", dest='lots', action="store", help="")
    parser.add_option("-r", "--info_dir", dest='info_dir', action="store", help="")
    parser.add_option("-i", "--input", dest='data_dir', action="store", help="")
    parser.add_option("-o", "--output", dest='save_dir', action="store", help="")
    parser.add_option("-c", "--column", dest='signal', action="store", help="", default='Median')
    parser.add_option('-v', '--verbose', dest='print', action='store_true', help='', default=False)

    (options, args) = parser.parse_args()

    save_dir = options.save_dir
    work_dir = os.path.join(options.save_dir, 'tmp')
    ut.check_dir(work_dir)
    log_dir = os.path.join(save_dir, 'data_cleaning_logs')
    ut.check_dir(log_dir)

    # extract
    extract_dir = os.path.join(work_dir, 'extract')
    options.save_dir = extract_dir
    ut.check_dir(extract_dir)

    print('...Data normalization')
    extract(options)
    print('Data normalization DONE!')

    # preproc
    prepoc_dir = os.path.join(work_dir, 'preproc')
    ut.check_dir(prepoc_dir)

    options.data_dir = extract_dir
    options.save_dir = prepoc_dir

    print('...Data preprocessing')
    prepoc(options, log_dir)
    print('Data preprocessing DONE!')

    # filtro 1
    filter1_dir = os.path.join(work_dir, 'filter1')
    ut.check_dir(filter1_dir)

    options.data_dir = extract_dir
    options.save_dir = filter1_dir
    preproc_data = os.path.join(prepoc_dir, 'preproc.tsv')

    print('...Filter 1')
    filter1(options, preproc_data)
    print('Filter 1 DONE!')

    # filter 2
    filter2_dir = os.path.join(work_dir, 'filter2')
    ut.check_dir(filter2_dir)

    options.data_dir = filter1_dir
    options.save_dir = filter2_dir

    print('...Filter 2')
    filter2(options, log_dir)
    print('Filter 2 DONE!')

    # filter 3

    filter3_dir = os.path.join(work_dir, 'filter3')
    ut.check_dir(filter3_dir)

    options.data_dir = filter2_dir
    options.save_dir = filter3_dir

    print('...Filter 3')
    filter3(options, log_dir, preproc_data)
    print('Filter 3 DONE!')

    # Data organization

    options.data_dir = filter3_dir
    options.save_dir = save_dir
    print('...Data organization')
    organize(options, preproc_data)
    print('Data organization DONE!')
    print('Removing temp files')
    shutil.rmtree(work_dir, ignore_errors=True)

    print('Process finished!')


if __name__ == '__main__':
    main()
