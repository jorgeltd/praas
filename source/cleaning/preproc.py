"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import numpy as np
import pandas as pd
import source.utils.helper as ut
import os
from scipy.special import erfc

# Este script hace varias cosas
# 1.- Calcula las medias de controles relevantes para cada sección y las guarda en una tabla
# 2.- Aplica cierta criterios para crear flag para cada seccion de un microarray indicando si la seccion es buena o mala
# 3.- Seleccionar el punto minimo de interaccion entre buffer y empty


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def _significance(serie):
    # Siginificacia
    # 0 = valor <  +- 2 desviaciones standard
    # 1 = valor >= +- 2 desviaciones standard
    # 2 = valor >= +- 3 desviaciones standard
    mean = serie.mean()
    std = serie.std(ddof=0)
    sig = pd.Series([0 for i in range(serie.shape[0])])
    sig.index = serie.index

    sig.loc[np.abs(serie - mean) >= 1 * std] = 1
    sig.loc[np.abs(serie - mean) >= 2 * std] = 2
    sig.loc[np.abs(serie - mean) >= 3 * std] = 3

    return sig


def _chauvenete_iter(series, iterations=2):

    outliers_list = series.copy()
    outliers_list.loc[:] = False

    for i in range(iterations):
        mean = series.mean()
        std = series.std(ddof=0)
        n = series.shape[0]
        outliers = series.apply(lambda x: erfc(np.abs(x - mean) / std) * n) < 0.5

        for x in outliers.index:
            outliers_list.loc[x] = outliers_list[x] or outliers[x]

        series = series[~outliers]

    return outliers_list


def _find_min(df):
    # Casos
    # buffer | empty | sig>0 | RESULTADO
    # 0         0       0       avg(emp,buff)
    # 0         1      any      buffer
    # 1         0      any      empty
    # 1         1       0       PEOR CASO, avg por el momento
    # 0         0       1       mas cercano media
    # 1         1       1       mas cercano media
    df = df.reset_index()
    buffer = df['Buffer_Outlier']
    empty = df['Empty_Outlier']
    sig = df['B-E_Sig'] > 0
    buff_mean = df['Buffer'].mean()
    emp_mean = df['Empty'].mean()
    val_buff = df['Buffer']
    val_emp = df['Empty']
    # lista caso_1
    avg = (val_emp + val_emp) / 2
    # lista caso_5
    close_eval = np.less(np.abs(val_buff - buff_mean), np.abs(val_emp - emp_mean))
    close_to_mean = np.where(close_eval, val_buff, val_emp)

    case_1 = (buffer == False) & (empty == False) & (sig == False)
    case_2 = (buffer == False) & (empty == True)
    case_3 = (buffer == True) & (empty == False)
    case_4 = (buffer == True) & (empty == True) & (sig == False)
    case_5 = (buffer == empty) & (sig == True)

    condlist = [case_1, case_2, case_3, case_4, case_5]
    condchoise = [avg, val_buff, val_emp, avg, close_to_mean]
    minimum = np.select(condlist, condchoise, default=-1)

    tmp = pd.DataFrame()
    tmp['1'] = case_1
    tmp['2'] = case_2
    tmp['3'] = case_3
    tmp['4'] = case_4
    tmp['5'] = case_5

    return minimum


def _eval_cases(df):
    # 6 = Anti-IGg1|Anti-IGg1|Anti-IGg1 < Buffer|Empty
    case_6 = (df['Anti-human-IgG1'] < df['Min']) | (df['Anti-human-IgG2'] < df['Min']) | (df['Anti-human-IgG3'] < df['Min'])
    return case_6


def prepoc(options, log_dir):

    # options.data_dir = sys.argv[1]
    # options.results_dir = sys.argv[2]
    ut.check_dir(options.save_dir)

    excluded_controls = ['AlexaAntiMouseAb']
    dfs_median = []
    dfs_mean = []

    for root, dirs, files in os.walk(options.data_dir):
        # Saltarse directorio sin archivos
        if len(files) == 0:
            continue

        ctrl_files = [x for x in files if 'ctrl' in x]
        lote = os.path.split(root)[-1]

        for file in ctrl_files:

            if options.print:
                print(file, lote)

            placa = file.split('_')[-1]
            df = pd.read_csv(os.path.join(root, file), sep='\t')

            # Borrar controles de no interes
            for control in excluded_controls:
                df = df[df['Protein'] != control]

            df_group = df.groupby(['Block', 'Protein'])

            df_data = df[['Block', 'Protein']].copy()
            df_data['Log2_Mean'] = df_group['Log2_Mean'].transform(np.mean)
            df_data['Log2_Median'] = df_group['Log2_Median'].transform(np.mean)
            df_data = df_data.drop_duplicates(['Block', 'Protein'])

            df_mean = df_data.drop(['Log2_Median'], axis=1)
            df_median = df_data.drop(['Log2_Mean'], axis=1)
            df_mean = df_mean.pivot(index='Block', columns='Protein', values='Log2_Mean')
            df_median = df_median.pivot(index='Block', columns='Protein', values='Log2_Median')

            # Esto es feo pero sirve
            tmp = df[df['Protein'] != 'Empty']
            tmp = tmp.reset_index(drop=True)
            tmp.loc[::2, 'Protein'] = tmp.loc[::2, 'Protein'] + '_1'
            tmp.loc[1::2, 'Protein'] = tmp.loc[1::2, 'Protein'] + '_2'
            log2_mean_values = tmp.pivot(index='Block', columns='Protein', values='Log2_Mean')
            log2_median_values = tmp.pivot(index='Block', columns='Protein', values='Log2_Median')

            df_mean = pd.concat([log2_mean_values, df_mean], axis=1)
            df_median = pd.concat([log2_median_values, df_median], axis=1)

            df_mean['Buffer-Empty'] = (df_mean['Buffer'] - df_mean['Empty'])
            df_mean['IG1-Buffer'] = (df_mean['HumanIgG1'] - df_mean['Buffer'])
            df_mean['IG1-Empty'] = (df_mean['HumanIgG1'] - df_mean['Empty'])
            df_mean['IG3-Buffer'] = (df_mean['HumanIgG3'] - df_mean['Buffer'])
            df_mean['IG3-Empty'] = (df_mean['HumanIgG3'] - df_mean['Empty'])
            df_mean['IG1-IG3'] = (df_mean['HumanIgG1'] - df_mean['HumanIgG3'])

            df_median['Buffer-Empty'] = (df_median['Buffer'] - df_median['Empty'])
            df_median['IG1-Buffer'] = (df_median['HumanIgG1'] - df_median['Buffer'])
            df_median['IG1-Empty'] = (df_median['HumanIgG1'] - df_median['Empty'])
            df_median['IG3-Buffer'] = (df_median['HumanIgG3'] - df_median['Buffer'])
            df_median['IG3-Empty'] = (df_median['HumanIgG3'] - df_median['Empty'])
            df_median['IG1-IG3'] = (df_median['HumanIgG1'] - df_median['HumanIgG3'])

            df_median['B-E_Sig'] = _significance(df_median['Buffer-Empty'])
            df_mean['B-E_Sig'] = _significance(df_mean['Buffer-Empty'])

            # Buscar outliers dentro de los promedios de los controles empty y buffer
            df_median['Buffer_Outlier'] = _chauvenete_iter(df_median['Buffer'], iterations=1)
            df_median['Empty_Outlier'] = _chauvenete_iter(df_median['Empty'], iterations=1)

            df_mean['Buffer_Outlier'] = _chauvenete_iter(df_mean['Buffer'], iterations=1)
            df_mean['Empty_Outlier'] = _chauvenete_iter(df_mean['Empty'], iterations=1)

            # Seleccionar punto de corte minimo entre buffer o empty
            df_median['Min'] = df_median.index
            df_mean['Min'] = df_mean.index

            df_median['Min'] = _find_min(df_median)
            df_mean['Min'] = _find_min(df_mean)

            # Columna flag es para marcar codiciones para eliminación
            # Si el valor es distinto a 0 se debe eliminar datos de la seccion
            # 1 = IG1 < Buffer|Empty
            # 2 = IG3 < Buffer|Empty
            # 3 = Anti-IGg1 > IG1|IG3
            # 4 = Anti-IGg2 > IG1|IG3
            # 5 = Anti-IGg3 > IG1|IG3
            # 6 = Anti-IGg1|Anti-IGg1|Anti-IGg1 < Buffer|Empty

            # Comparaciones deberian haber sido hechas con funciones basicas de pandas
            # df_mean['Flag_1'] = np.where((df_mean['IG1-Buffer'] < 0) | (df_mean['IG1-Empty'] < 0), 1, 0)
            # df_mean['Flag_2'] = np.where((df_mean['IG3-Buffer'] < 0) | (df_mean['IG3-Empty'] < 0), 1, 0)
            df_mean['Flag_1'] = np.where(df_mean['HumanIgG1'] < df_mean['Min'], 1, 0)
            df_mean['Flag_2'] = np.where(df_mean['HumanIgG3'] < df_mean['Min'], 1, 0)
            df_mean['Flag_3'] = np.where(np.greater(df_mean['Anti-human-IgG1'], df_mean['HumanIgG1']) |
                                         np.greater(df_mean['Anti-human-IgG1'], df_mean['HumanIgG3']), 1, 0)
            df_mean['Flag_4'] = np.where(np.greater(df_mean['Anti-human-IgG2'], df_mean['HumanIgG1']) |
                                         np.greater(df_mean['Anti-human-IgG2'], df_mean['HumanIgG3']), 1, 0)
            df_mean['Flag_5'] = np.where(np.greater(df_mean['Anti-human-IgG3'], df_mean['HumanIgG1']) |
                                         np.greater(df_mean['Anti-human-IgG3'], df_mean['HumanIgG3']), 1, 0)
            df_mean['Flag_6'] = _eval_cases(df_mean)

            df_mean['Flag_Count'] = df_mean[['Flag_1', 'Flag_2', 'Flag_3', 'Flag_4', 'Flag_5', 'Flag_6']].sum(axis=1)

            # df_median['Flag_1'] = np.where((df_median['IG1-Buffer'] < 0) | (df_median['IG1-Empty'] < 0), 1, 0)
            # df_median['Flag_2'] = np.where((df_median['IG3-Buffer'] < 0) | (df_median['IG3-Empty'] < 0), 1, 0)
            df_median['Flag_1'] = np.where(df_median['HumanIgG1'] < df_median['Min'], 1, 0)
            df_median['Flag_2'] = np.where(df_median['HumanIgG3'] < df_median['Min'], 1, 0)
            df_median['Flag_3'] = np.where(np.greater(df_median['Anti-human-IgG1'], df_median['HumanIgG1']) |
                                           np.greater(df_median['Anti-human-IgG1'], df_median['HumanIgG3']), 1, 0)
            df_median['Flag_4'] = np.where(np.greater(df_median['Anti-human-IgG2'], df_median['HumanIgG1']) |
                                           np.greater(df_median['Anti-human-IgG2'], df_median['HumanIgG3']), 1, 0)
            df_median['Flag_5'] = np.where(np.greater(df_median['Anti-human-IgG3'], df_median['HumanIgG1']) |
                                           np.greater(df_median['Anti-human-IgG3'], df_median['HumanIgG3']), 1, 0)
            df_median['Flag_6'] = _eval_cases(df_median)
            df_median['Flag_Count'] = df_median[['Flag_1', 'Flag_2', 'Flag_3', 'Flag_4', 'Flag_5', 'Flag_6']].sum(axis=1)

            # concatenar tabla a la tabla final
            df_median['File'] = df_mean['File'] = file.split('_')[-1]
            dfs_mean.append(df_mean)
            dfs_median.append(df_median)

            # df_mean.to_csv(os.path.join(results_dir, placa), sep='\t', float_format='%.6f')
            # df_median.to_csv(os.path.join(results_dir_median, placa), sep='\t', float_format='%.6f')

    # Guardar resultados
    median = pd.concat(dfs_median)
    mean = pd.concat(dfs_mean)
    mean.to_csv(os.path.join(options.save_dir, 'preproc.tsv'), sep='\t', float_format='%.6f')

    # Tablas sumario
    mean_flag_count = mean[mean['Flag_Count'] > 0].shape[0]
    median_flag_count = median[median['Flag_Count'] > 0].shape[0]

    index = ['IG1 < Buffer|Empty', 'IG3 < Buffer|Empty', 'Anti-IGg1 > IG1|IG3', 'Anti-IGg2 > IG1|IG3', 'Anti-IGg3 > IG1|IG3',
             'Anti-IGgX < Min']

    median_summary = median[['Flag_1', 'Flag_2', 'Flag_3', 'Flag_4', 'Flag_5', 'Flag_6']].apply(np.sum)
    median_summary.index = index
    median_summary.columns = ['Cantidad_Secciones']
    median_summary = median_summary.append(pd.DataFrame([median_flag_count], index=['Total']))

    median_summary.to_csv(os.path.join(log_dir, 'quality_summary.tsv'), sep='\t')
