"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import numpy as np
import pandas as pd
import os
import source.utils.helper as ut


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def filter1(options, preproc_data):

    #options.data_dir = sys.argv[1]
    #preproc_data = sys.argv[2]
    #options.save_dir = sys.argv[3]

    ut.check_dir(options.save_dir)
    df_filter = pd.read_csv(preproc_data, sep='\t')

    for root, dirs, files in os.walk(options.data_dir):
        lote = root.split('/')[-1]
        for file in files:
            microarray = file.split('_')[-1]

            if options.print:
                print(file, lote)

            df = pd.read_csv(os.path.join(root, file), sep='\t')

            df_microarray = df_filter[df_filter['File'] == microarray]
            is_bad = np.where(df_microarray['Flag_Count'] > 0, True, False)
            dict_map = dict(zip(df_microarray['Block'], is_bad))
            df['Bad_Block'] = df['Block'].map(dict_map)

            df.to_csv(os.path.join(options.save_dir, file), sep='\t', index=False, float_format='%.6f')
