"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""


import pandas as pd
import numpy as np
import os
import source.utils.helper as ut
from scipy.special import erfc

# Aplicar sobre una carpeta generada por el filtro 1
# Entrada 1 = carpeta datos, 2 = carpeta resultado
# Para la marcar outliers se usa el criterio de chauvenete sobre los erroes Log2_Median_L1_noAbs porque tienen una forma normal


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def _chauvenet(series):
    mean = series.mean()
    std = series.std(ddof=0)
    func = lambda x: erfc(np.abs(x - mean) / std) * series.shape[0]
    outliers = series.apply(func) < 0.5
    return outliers


def _chauvenete_iter(series, iter=5):

    outliers_list = series.copy()
    outliers_list.loc[:] = False

    for i in range(iter):
        mean = series.mean()
        std = series.std(ddof=0)
        n = series.shape[0]
        outliers = series.apply(lambda x: erfc(np.abs(x - mean) / std) * n) < 0.5

        for x in outliers.index:
            outliers_list.loc[x] = outliers_list[x] or outliers[x]

        series = series[~outliers]

    return outliers_list


def _get_stats(df, group, unit_error, flag):
    tmp = df.loc[group.index]
    count = tmp[tmp[flag] == True].shape[0]
    tmp = tmp[(tmp[flag] == False) | (tmp[flag].isnull())]
    mean = tmp[unit_error].mean()
    std = tmp[unit_error].std(ddof=0)
    minimum = tmp[unit_error].min()
    return mean, std, count, minimum


def filter2(options, log_dir):

    #options.data_dir = sys.argv[1]
    #log_dir = sys.argv[2]
    #options.save_dir = sys.argv[3]
    ut.check_dir(options.save_dir)
    ut.check_dir(log_dir)

    sum_median = []
    sum_mean = []
    sum_columns = ['File', 'Block', 'ID', 'Avg_Init', 'Std_Init', 'Avg_Final', 'Std_Final', 'Min_Final', 'Outliers']
    median_column = 'Log2_Median_L1'
    mean_column = 'Log2_Mean_L1'

    for root, dirs, files in os.walk(options.data_dir):

        # copiar archivos que no se modifican
        #other_files = [x for x in files if 'error' not in x and 'protein' not in x]
        #for file in other_files:
        #    copy2(os.path.join(root, file), options.save_dir)

        # iniciar proceso limpieza
        error_files = [x for x in files if 'error' in x]

        for error_file in error_files:
            microarray = error_file.split('_')[-1]

            if options.print:
                print(microarray)

            df_error = pd.read_csv(os.path.join(root, error_file), sep='\t')
            df_protein = pd.read_csv(os.path.join(root, 'proteins_' + microarray), sep='\t')

            # Valores iniciales para columnas de outliers
            df_error['Median_Error_Outlier'] = df_error['Mean_Error_Outlier'] = np.NaN

            # Excluir valores que esten en un grupo malo y agrupar para calcular outliers por bloque
            block_group = df_error[~df_error['Bad_Block']].groupby('Block')

            # Iterar por bloque del archivo y calcular los outliers
            for block, group in block_group:

                # Estadistica previa al calulo
                median_avg_init, median_std_init, c, m = _get_stats(df_error, group, median_column, 'Median_Error_Outlier')
                mean_avg_init, mean_std_init, c, m = _get_stats(df_error, group, mean_column, 'Mean_Error_Outlier')

                # Marcar outliers
                df_error.loc[group.index, 'Median_Error_Outlier'] = _chauvenete_iter(group[median_column])
                df_error.loc[group.index, 'Mean_Error_Outlier'] = _chauvenete_iter(group[mean_column])

                # Estadisticas sin outliers
                median_avg_final, median_std_final, median_outliers, min_median = _get_stats(df_error, group, median_column, 'Median_Error_Outlier')
                mean_avg_final, mean_std_final, mean_outliers, min_mean = _get_stats(df_error, group, mean_column, 'Mean_Error_Outlier')

                # Sumario de limpieza
                # HEADER ['File', 'Block', 'ID', 'Avg_Init', 'Std_Init', 'Avg_Final', 'Std_Final', Min_Final, 'Outliers']
                sum_median_row = [microarray, block, df_error['ID'].any(), median_avg_init, median_std_init,
                                  median_avg_final, median_std_final, min_median, median_outliers]
                sum_mean_row = [microarray, block, df_error['ID'].any(), mean_avg_init, mean_std_init,
                                mean_avg_final, mean_std_final, min_mean, mean_outliers]
                sum_median.append(sum_median_row)
                sum_mean.append(sum_mean_row)

            # old chauvenete por archivo completo
            # df_error.loc[~df_error['Bad_Block'], 'Median_Error_Outlier'] = _chauvenete_iter(df_error[~df_error['Bad_Block']][median_column])
            # df_error.loc[~df_error['Bad_Block'], 'Mean_Error_Outlier'] = _chauvenete_iter(df_error[~df_error['Bad_Block']][mean_column])

            # No encontre otra forma de copiar los outliers de df_error a df_protein
            df_protein['Median_Error_Outlier'] = df_protein['Mean_Error_Outlier'] = df_protein.index

            df_1 = df_error[['Median_Error_Outlier', 'Mean_Error_Outlier']].copy()
            df_2 = df_error[['Median_Error_Outlier', 'Mean_Error_Outlier']].copy()
            df_1.index = range(0, df_protein.shape[0], 2)
            df_2.index = range(1, df_protein.shape[0], 2)
            df_map = pd.concat([df_1, df_2]).sort_index()

            df_protein[['Median_Error_Outlier', 'Mean_Error_Outlier']] = df_map

            # Guardar archivos de datos con los outliers marcados
            df_protein.to_csv(os.path.join(options.save_dir, 'proteins_' + microarray), sep='\t', index=False, float_format='%.6f')
            df_error.to_csv(os.path.join(options.save_dir, error_file), sep='\t', index=False, float_format='%.6f')

    # Guardar sumario del filtro aplicado
    df_sum_median = pd.DataFrame(sum_median, columns=sum_columns)
    # TODO ver esta linea
    df_sum_median.to_csv(os.path.join(log_dir, 'filter2_summary_Median_Error_Outlier.tsv'), index=False, sep='\t', float_format='%.6f')
    df_sum_mean = pd.DataFrame(sum_mean, columns=sum_columns)
    # df_sum_mean.to_csv(os.path.join(log_dir, sys.argv[2] + '_summary_Mean_Error_Outlier.tsv'), index=False, sep='\t', float_format='%.6f')

