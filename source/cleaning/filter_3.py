"""
Namefile.py

Description

Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import pandas as pd
import numpy as np
import os
import source.utils.helper as ut


# PARAMETERS = ../data_cleaned/filtro_2_median/ .
#
#
# sv filtro_3_median_log2_median/

__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def filter3(options, log_dir, preproc_data):

    #options.data_dir = sys.argv[1]
    #preproc_data = sys.argv[2]
    #log_dir = sys.argv[3]
    #options.save_dir = sys.argv[4]

    ut.check_dir(options.save_dir)
    ut.check_dir(log_dir)

    key = 'Log2_Median'
    error_type = 'Median_Error_Outlier'
    summary_data = []
    summary_header = ['File', 'Block', 'Discarted_Count']

    df_flags = pd.read_csv(preproc_data, sep='\t')

    for root, dirs, files in os.walk(options.data_dir):
        protein_files = [x for x in files if 'protein' in x]
        other_files = [x for x in files if x not in protein_files and 'all' not in x]
        # print(other_files)
        #for file in other_files:
        #    copy2(os.path.join(root, file), options.save_dir)

        for file in protein_files:
            microarray = file.split('_')[-1]

            if options.print:
                print(microarray)

            df = pd.read_csv(os.path.join(root, file), sep='\t')

            # Reducir los datos de duplicados a datos unicos mediante promedios
            sample_1 = df.iloc[::2].reset_index(drop=True)
            sample_2 = df.iloc[1::2].reset_index(drop=True)

            tmp = sample_1.loc[:, ['Block', 'Protein', 'ID', 'Protein_Sequence', 'Protein_Len', 'Antibody',
                                   'Antibody_Ncl_Seq', 'Bad_Block', error_type]]
            tmp['Log2_Median'] = (sample_1['Log2_Median'] + sample_2['Log2_Median']) / 2
            tmp['Log2_Mean'] = (sample_1['Log2_Mean'] + sample_2['Log2_Mean']) / 2
            df = tmp

            groupby = df.groupby('Block')

            for block, group in groupby:
                group = group[(group['Bad_Block'] == False) & (group['Median_Error_Outlier'] == False)]
                minimum = df_flags.loc[(df_flags['File'] == microarray) & (df_flags['Block'] == block), 'Min'].tolist()[0]
                df.loc[group.index, key + '_Under_Min'] = group[key] < minimum
                data_row = [microarray, block, np.sum((group[key] < minimum))]
                df.loc[group.index, key] = group[key] - minimum
                summary_data.append(data_row)
            df.to_csv(os.path.join(options.save_dir, file), sep='\t', index=False, float_format='%.6f')

    df_summary = pd.DataFrame(summary_data, columns=summary_header)
    df_summary.to_csv(os.path.join(log_dir, 'filtro_3_summary_discarted_by_{}.tsv'.format(key)), index=False, sep='\t')
