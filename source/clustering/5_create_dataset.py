"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import pandas as pd
import sys


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


#funcion que permite contar la frecuencia de un residuo
def countFreqResidue(seq, residue):

    count=0
    for res in seq:
        if res == residue:
            count+=1
    return count

data = pd.read_csv(sys.argv[1])

fileData = open('antigen_filter.fasta', 'w')
fileData2 = open('antigen_filter_seq.txt', 'w')

for i in range(len(data)):
    antigen = str(data['antigen'][i])
    seq = str(data['seq'][i])
    seq = seq.replace(".", "")
    seq = seq.replace("?", "")

    fileData.write(">"+antigen+"\n")
    fileData.write(seq+"\n")
    fileData2.write(seq+"\n")
fileData.close()
fileData2.close()

#hacemos un set de datos con la frecuencia de los residuos y el largo de los elementos
matrixResponse = []
matrixAntigenID = []

listAA = "ARNDCQEGHILKMFPSTWYV"

print(len(listAA))

for i in range(len(data)):
    try:
        row = []
        for element in listAA:
            row.append(countFreqResidue(data['seq'][i], element))

        row.append(len(data['seq'][i]))
        matrixResponse.append(row)
        matrixAntigenID.append([data['antigen'][i]])
    except:
        pass
columnsHeader = list(listAA)
columnsHeader.append("length")

dataFreq = pd.DataFrame(matrixResponse, columns=columnsHeader)
dataFreqIndex = pd.DataFrame(matrixAntigenID, columns=['antigenID'])

dataFreq.to_csv("dataSetFrequence.csv", index=False)
dataFreqIndex.to_csv("dataSetFrequenceIndex.csv", index=False)
