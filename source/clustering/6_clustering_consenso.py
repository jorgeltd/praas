"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""

import pandas as pd
import sys
import itertools
import os
import numpy as np
import numpy as np
import matplotlib.pyplot as plt
from matplotlib_venn import venn2
import seaborn as sns


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def _get_data(_df, type_data):
    return _df[(_df['Type'] == type_data) & (_df['Bad_Data'] == False)]


def _get_cluster(_df, _antigens):
    _df = _df.set_index('Antigen')
    _df = _df.loc[_antigens, :]
    return _df


# 1 = cluster grafos similitud
# 2 = cluster frecuencias aminoacidos
# 3 = directorio resultados
# 4 = dataset

name_a = os.path.split(sys.argv[1])[-1].split('.')[0]
name_b = os.path.split(sys.argv[2])[-1].split('.')[0]

venn_dir = os.path.join(sys.argv[3], '{}_{}_ven_diagrams'.format(name_a, name_b))
if not os.path.exists(venn_dir):
    os.mkdir(venn_dir)

cluster_A_path = sys.argv[1]
cluster_B_path = sys.argv[2]
data = []
data_header = ['A:' + sys.argv[1], 'B:' + sys.argv[2], 'A_Size', 'B_Size', 'A_in_B', 'B_in_A']
df_A = pd.read_csv(cluster_A_path)
df_B = pd.read_csv(cluster_B_path)

groups_A = df_A.groupby('Group')
groups_B = df_B.groupby('Group')

sets_A = []
sets_B = []

for cluster, gp in groups_A:
    sets_A.append((cluster, gp['Antigen'].values))

for cluster, gp in groups_B:
    sets_B.append((cluster, gp['Antigen'].values))

cl_id = -1
cluster_list = []

for set_a in sets_A:
    for set_b in sets_B:
        eq = 0
        cl_id += 1
        for x in set_a[1]:
            for y in set_b[1]:
                if x == y:
                    eq += 1
                    cluster_list.append([x, cl_id])
        a_in_b = eq / len(set_a[1])
        b_in_a = eq / len(set_b[1])
        data.append([set_a[0], set_b[0], len(set_a[1]), len(set_b[1]), a_in_b, b_in_a])

        # Diagramas venn
        venn2([set(set_a[1]), set(set_b[1])], set_labels=('Louvain Cluster {}'.format(set_a[0]), 'K-Means Cluster {}'.format(set_b[0])))
        plt.savefig(os.path.join(venn_dir, '{}_{}_clusters_{}-{}.png').format(name_a, name_b, set_a[0], set_b[0]))
        plt.close(fig=plt.gcf())
        # HACER COMPARACIONES ENTRE SETS
'''
CLUSTER ID=40 MAXIMO CON 172 ELEMENTOS
'''
# agregar clusters al set de datos y calcular z-scores por grupos de antigenos

df_cluster = pd.DataFrame(cluster_list, columns=['Antigen', 'Cluster_By_Antigen'])
cluster_size = df_cluster.groupby('Cluster_By_Antigen').size()
cluster_size.index = cluster_size.index.astype(float)
df_cluster.to_csv(os.path.join(sys.argv[3], 'cluster_unified_{}_{}.csv'.format(name_a, name_b)), index=False)

df_all = _get_data(pd.read_csv(sys.argv[4]), 'Antigen')
groupby = df_all.groupby('Antigen')

i = 0
for antigen, df in groupby:
    if i % 500 == 0:
        print(i)
    i += 1

    if df.shape[0] == 0:
        continue
    mean = df['Median_Value'].mean()
    std = df['Median_Value'].std(ddof=0)
    z_score = df['Median_Value'].apply(lambda x: (x - mean) / std)
    # value = np.where(z_score > 2, 'Strong', np.where(z_score < 2, 'Weak', np.nan))
    df_all.loc[df.index, 'Z_Score_By_Antigen'] = z_score

data_savedir = os.path.join(sys.argv[3], 'cluster_data')

if not os.path.exists(data_savedir):
    os.mkdir(data_savedir)

df_all['Z_Class_By_Antigen'] = np.where(df_all['Z_Score_By_Antigen'] > 2, 'Strong',
                                        np.where(df_all['Z_Score_By_Antigen'] < -2, 'Weak', ''))
# Añadir clusters al set de datos
mapper = df_cluster.set_index('Antigen', drop=True)['Cluster_By_Antigen']
df_all['Cluster'] = df_all['Antigen'].map(mapper, na_action='ignore')
df_all = df_all[df_all['Cluster'].notnull()]

df_all = df_all[['Antibody', 'Antigen', 'Median_Value', 'Z-Score', 'Cluster', 'Bad_Data', 'Antigen_Seq', 'Antibody_Ncl_Seq']]
df_all.to_csv(os.path.join(data_savedir, 'dataset_clustering.csv'), index=False, float_format='%.6f')

# HISTOGRAMAS CLUSTERS
gp_cluster = df_all.groupby('Cluster')

for cluster, df in gp_cluster:
    sns.distplot(df['Median_Value'], kde=False)
    plt.xlabel('Señal Normalizada')
    plt.ylabel('Frecuencia')
    plt.suptitle('Grupo CL_{0:.0f}'.format(cluster))
    plt.savefig(os.path.join(data_savedir, 'cluster_{}.png'.format(cluster)))
    plt.close(fig=plt.gcf())
    print(cluster)


# INFO CLUSTERS
ex_per_clust = df_all.groupby('Cluster').size()
df_best = df_all[df_all['Z-Score'] >= 2]

best_clusters = df_best.groupby('Cluster').size().sort_values(ascending=False)
best_clusters = pd.DataFrame(best_clusters).rename(columns={0: 'X>2 Z Count'})
best_clusters['Ex_Per_Cluster'] = best_clusters['Antigens_Per_Cluster'] = best_clusters.index
best_clusters['Ex_Per_Cluster'] = best_clusters['Ex_Per_Cluster'].map(ex_per_clust)
best_clusters['Antigens_Per_Cluster'] = best_clusters['Antigens_Per_Cluster'].map(cluster_size)


best_clusters.to_csv(os.path.join(data_savedir, 'cluster_info_{}_{}.csv'.format(name_a, name_b)))

# COMPARAR ANTICUERPOS QUE SE REPITEN EN INTERACCIONES FUERTES

data = df_all[(df_all['Cluster'] == 37) & (df_all['Z-Score'] >= 2)]
antibodies = list(data['Antibody'].unique())
antigens = list(data['Antigen'].unique())
groupby = data.groupby('Antibody')
keys = list(groupby.groups.keys())
# agregar clusters al set de datos y calcular z-scores por

gp_size = groupby.size()

# Estadisticas de los clusters
df = pd.DataFrame(data, columns=data_header)
df['Max'] = np.where(df['A_in_B'] > df['B_in_A'], df['A_in_B'], df['B_in_A'])

median = df['Max'].median()
mean = df['Max'].mean()
std = df['Max'].std(ddof=0)
maxi = df['Max'].max()
minin = df['Max'].min()

df['Stats'] = np.nan
df.loc[0, 'Stats'] = 'median={}'.format(median)
df.loc[1, 'Stats'] = 'mean={}'.format(mean)
df.loc[2, 'Stats'] = 'std={}'.format(std)
df.loc[3, 'Stats'] = 'median={}'.format(minin)
df.loc[4, 'Stats'] = 'median={}'.format(maxi)

filename = '{}_{}_validation.csv'.format(name_a, name_b)
# df.to_csv(os.path.join(sys.argv[3], filename), float_format='%.6f', index=False)

print('Done')
