# Funciones varias
import os

lot_1 = 'HA20073'
lot_2 = 'HA20087'

controls = ['AlexaAntiMouseAb~N/A', 'MAPKAP~N/A', 'BSA3~N/A', 'Anti-human-IgG1~N/A',
            'Anti-human-IgG2~N/A', 'Anti-human-IgG3~N/A', 'Kinase1-1', 'Calmodulin2', 'GST3', 'GST4', 'GST5', 'GST6',
            'GST7', 'GST8', 'BiotinAb9~N/A', 'AntiBiotinAb~N/A', 'V5Control2~N/A', 'CMK2A~N/A', 'HumanIgG3~N/A',
            'HumanIgG1~N/A', 'Buffer', 'Empty']

controls2 = ['AlexaAntiMouseAb', 'MAPKAP', 'BSA3', 'Anti-human-IgG1',
            'Anti-human-IgG2', 'Anti-human-IgG3', 'Kinase1-1', 'Calmodulin2', 'GST3', 'GST4', 'GST5', 'GST6',
            'GST7', 'GST8', 'BiotinAb9', 'AntiBiotinAb', 'V5Control2~N/A', 'CMK2A~N/A', 'HumanIgG3',
            'HumanIgG1', 'Buffer', 'Empty']


def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts


def check_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def name_from_file(file):
    return file.split('_')[-1].split('.')[0]
