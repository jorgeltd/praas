"""
Namefile.py

Description


Copyright (C) 2019  Jorge Torres-Almonacid, jorge.torres@umag.cl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
"""


import pandas as pd
import numpy as np
import os
import itertools
import shutil
import math
import time
import sys
import csv
import source.utils.helper as ut
from optparse import OptionParser
from scipy import stats
from scipy.special import comb
from mpi4py import MPI
from source.clustering.mpi_data_concat import concat_data


__author__ = "Jorge Torres-Almonacid"
__license__ = "GPL"
__version__ = "3.0"
__maintainer__ = "Jorge Torres-Almonacid"
__email__ = "jorge.torres@umag.cl"


def _get_data(df, ty):
    return df[(df['Type'] == ty) & (df['Bad_Data'] == False)]


def _u_test(s1, s2, sig):
    u, p = stats.mannwhitneyu(s1, s2, alternative='two-sided')
    sts = 0
    if p < sig:
        sts = 1

    return u, p, sts


def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]


def split(anti_list, n_parts):
    l_ = [(x, y) for x, y in itertools.combinations(anti_list, 2)]
    return [l_[i::n_parts] for i in range(n_parts)]


# srun -N 2 -n 40 python script.py results_log dataset/all.csv 34
def main():

    parser = OptionParser()
    parser.add_option("-i", "--input", dest='input', action="store", help="")
    parser.add_option('-o', '--output', dest='output', action='store', help='')
    parser.add_option('-n', '--minimun_antigens', dest='n_antigens', action='store', help='')
    (options, args) = parser.parse_args()
    options.n_antigens = int(options.n_antigens)

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    #print('Proc {} reading data'.format(rank))
    df = _get_data(pd.read_csv(options.input), 'Antigen')
    #print('Proc {} read done!'.format(rank))

    if rank == 0:
        work_dir = os.path.join(options.output, 'tmp')
        ut.check_dir(work_dir)
        print('Dividing and init jobs...')
        count = df.groupby(['Antigen']).size().reset_index(name='Count')
        antigen_list = count[count['Count'] >= options.n_antigens]['Antigen'].values
        job = split(antigen_list, size)
        bonfer_coef = comb(len(antigen_list), 2)
    else:
        job = None
        bonfer_coef = None
        work_dir = None

    job = comm.scatter(job, root=0)

    work_dir = comm.bcast(work_dir, root=0)
    bonfer_coef = comm.bcast(bonfer_coef, root=0)

    file = open(os.path.join(work_dir, 'proc_{}_data.csv'.format(rank)), 'w')
    file_header = ['A', 'B', 'U_Test', 'P_Value']
    file_writer = csv.DictWriter(file, fieldnames=file_header)
    file_writer.writeheader()

    # print('Proc {} job init'.format(rank))
    # print(bonfer_coef)
    for i, j in job:
        s1 = df[df['Antigen'] == i]['Median_Value']
        s2 = df[df['Antigen'] == j]['Median_Value']
        u, p = stats.mannwhitneyu(s1, s2, alternative='two-sided')
        file_writer.writerow({'A': i, 'B': j, 'U_Test': u, 'P_Value': p})

    file.close()

    # TODO incorporar data cat en este archivo
    comm.Barrier()

    if rank == 0:
        print('U-Test Done!... Concatenating data')
        # incluir codigo de cata
        concat_data(work_dir, options.output)
        shutil.rmtree(work_dir, ignore_errors=True)


if __name__ == '__main__':
    main()
